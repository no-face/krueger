#!/usr/bin/env python
# -*- coding: utf-8 -*-

from conans import ConanFile, tools, AutoToolsBuildEnvironment

import os
from os import path

class Recipe(ConanFile):
    name        = "krueger"
    version     = "0.0.1"
    description = "Serialization library for c++11"
    license     = "MIT"
    url         = ""

    settings = "os", "compiler", "build_type", "arch"

    #Conan dependencies
    requires = (
        "jsoncpp/1.8.0@theirix/stable",
        "commonex/1.0.0@noface/testing",

        "waf/0.1.1@noface/stable",
        "WafGenerator/0.0.4@noface/testing"
    )
    dev_requires = (
        "catch/1.5.0@TyRoXx/stable",
    )

    generators = "Waf"
    exports_sources = (
        "wscript",
        "src/**",
        "test/**",
        "LICENSE.md"
    )
    options = {
        "shared"            : [True, False]
    }
    default_options     = (
        'shared=False',
    )

    def imports(self):
        # Copy waf executable to project folder
        self.copy("waf", dst=".")

        self.copy("*.dll", dst="bin", src="bin")    # From bin to bin
        self.copy("*.dylib*", dst="bin", src="lib") # From lib to bin

    def build(self):
        self.build_path = path.abspath("build")

        self.run(
            "waf configure build install -o %s %s" % (self.build_path, self.get_options()),
            cwd=self.conanfile_directory)


    def package_info(self):
        self.cpp_info.libs    = ['krueger']

    def get_options(self):
        opts = []

        if self.settings.build_type == "Debug":
            opts.append("--debug")
        else:
            opts.append("--release")

        if self.options.shared:
            self.output.info("building shared library")
            opts.append("--shared")

        if not hasattr(self, "package_folder"):
            self.package_folder = path.abspath(path.join(".", "package"))

        opts.append("--prefix=%s" % self.package_folder)

        return " ".join(opts)
