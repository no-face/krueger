#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from os import path
from waflib.Configure import conf

NAME = 'krueger'

top='.'
out='build'

def load_tools(ctx):
    ctx.load('compiler_c compiler_cxx')
    ctx.load('gnu_dirs')

def options(ctx):
    load_tools(ctx)

    ctx.add_option('--shared', action='store_true', default=False, help='Build libs as shared libraries')

def configure(ctx):
    load_tools(ctx)

    ctx.load('conanbuildinfo_waf', tooldir=[".", path.join(ctx.bldnode.abspath(), "..")]);

    ctx.env.CXXFLAGS = ['-std=c++11', '-g']
    ctx.env.CFLAGS = ['-g']
    ctx.env.LDFLAGS = ['-g']

def build(ctx):
    ctx.recurse('src')

######################################## Helpers ############################################################

@conf
def find_files(ctx, *k):
    return ctx.path.ant_glob(k, src=True,dir=False)

@conf
def lib(bld, *k, **kw):
    from waflib import Utils

    if 'install_path' not in kw:
        kw['install_path'] = bld.env.LIBDIR

    if bld.env.shared:
        bld.shlib(*k, **kw)
    else:
        bld.stlib(*k, **kw)

    includedir = kw.get('install_includedir', bld.env.INCLUDEDIR)

    install_headers = []

    if 'install_headers' in kw:
        install_headers = kw['install_headers']
    elif 'export_includes' in kw:

        for inc_dir in kw.get('export_includes'):

            inc_node = bld.path.make_node(inc_dir)
            incs = inc_node.ant_glob([
                                path.join('**', '*.h'),
                                path.join('**', '*.hpp')])

            install_headers.extend(incs)

    if install_headers:
        headers_base = kw.get("headers_base", None)
        relative_trick = kw.get("install_relative", True)
        bld.install_files(
            includedir,
            Utils.to_list(install_headers),
            relative_trick=relative_trick,
            cwd=headers_base)
