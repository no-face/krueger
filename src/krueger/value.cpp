#include "value.h"

#include <sstream>

#include "exceptions.h"

#include "object.h"

namespace krueger {

value::Data::Data(const value::Data &other)
    : Data()
{
    copy(other);
}

value::Data &value::Data::operator=(const value::Data &other){
    copy(other);

    return *this;
}

void value::Data::copy(const value::Data &other){
    this->number = other.number;
    if(!other.strValue.empty()){
        this->strValue = other.strValue;
    }
    else if(other.object){
        if(!this->object){
            this->object = std::make_shared<krueger::object>(*other.object);
        }
        else{
            *this->object = *other.object;
        }
    }
}

value::value(const char *cstr)
    : value(std::string(cstr))
{}

value::value(std::string &&str)
    : _type(Type::text)
{
    data.strValue = std::move(str);
}

value::value(const std::string &str)
    : _type(Type::text)
{
    data.strValue = str;
}

value::value(const object &obj)
    : _type(Type::object)
{
    data.object = std::make_shared<object>(obj);
}

value::value(object &&obj)
    : _type(Type::object)
{
    data.object = std::make_shared<object>(std::move(obj));
}

value::value(std::nullptr_t)
    : value()
{}

value::value(bool b)
    : _type(Type::boolean)
{
    data.number.boolean = b;
}

value::value(integer_t i)
    : _type(Type::integer)
{
    data.number.integer = i;
}

value::value(real_t r)
    : _type(Type::real)
{
    data.number.real = r;
}

value::Type value::type() const{
    return _type;
}

bool value::is(value::Type t) const{
    return type() == t;
}

bool value::empty() const{
    return _type == Type::null;
}

std::ostream &operator<<(std::ostream &out, const value &v){
    v.streamTo(out);
    return out;
}

void value::streamTo(std::ostream &out) const{
    switch (type()) {
    case Type::integer  : out << data.number.integer; break;
    default             : out << operator std::string();
    }
}

bool value::operator==(const std::string &other) const{
    return is(Type::text) && str() == other;
}

bool value::operator==(const char *other) const{
    return operator ==(std::string(other));
}

bool value::operator==(integer_t other) const{
    return is(Type::integer) && integer() == other;
}

bool value::operator==(double other) const{
    return is(Type::real) && real() == other;
}

bool value::operator==(bool other) const{
    return is(Type::boolean) && boolean() == other;
}

bool value::operator ==(const value &other) const{
    if(type() != other.type()){
        return false;
    }

    switch (type()) {
    case Type::boolean: return boolean() == other.boolean();
    case Type::integer: return integer() == other.integer();
    case Type::real   : return real()    == other.real();
    case Type::text   : return str() == other.str();
    case Type::object : return obj() == other.obj();
    default:
        return false; //maybe throw exception
    }
}

value::integer_t value::to_integer() const{
    switch (_type) {
    case Type::integer  : return data.number.integer;
    case Type::real     : return data.number.real;
    case Type::boolean  : return data.number.boolean;
    case Type::null     : return 0;
    case Type::text     : return text_to_integer(data.strValue);
    default:
        return 0; //throw
    }
}

value::real_t value::to_real() const{
    switch (_type) {
    case Type::real     : return data.number.real;
    case Type::text     : return text_to_real(data.strValue);
    default:
        return to_integer();
    }
}

float value::to_float() const{
    return to_real();
}

double value::to_double() const{
    return to_real();
}

bool value::to_bool() const{
    switch (type()) {
    case Type::boolean  : return data.number.boolean;
    case Type::text     : return text_to_bool(data.strValue);
    default:
        return operator int(); //fallbacks to int conversion
    }
}

std::string value::to_string() const{
    switch (_type) {
    case Type::text     : return data.strValue;
    case Type::integer  : return std::to_string(data.number.integer);
    case Type::real     : return real_to_string(data.number.real);
    case Type::boolean  : return bool_to_string(data.number.boolean);
    case Type::null     : return "null";
    default:
        return {};
    }
}

const std::string &value::str() const{
    return data.strValue;
}

value::real_t value::real() const{
    return data.number.real;
}

long long value::integer() const{
    return data.number.integer;
}

bool value::boolean() const{
    return data.number.boolean;
}

object & value::obj(){
    return get_obj();
}

const object & value::obj() const{
    return const_cast<value*>(this)->get_obj();
}

object &value::get_obj(){
    if(!data.object){
        throw IllegalStateException("trying to get object, but it is null");
    }

    return *data.object;
}

bool value::text_to_bool(const  std::string & str) const{
    bool b;
    std::stringstream in{str};
    in >> std::boolalpha >> b;

    if(in.fail()){
        throw CastException("Failed to convert '" + str + "' to bool");
    }

    return b;
}

value::real_t value::text_to_real(const std::string &str) const{
    try{
        return std::stod(str);
    }
    catch(const std::invalid_argument & ){
        throw CastException("Failed to convert '" + str + "' to double");
    }
    catch(...){
        throw;
    }
}

value::integer_t value::text_to_integer(const std::string &str) const{
    try{
        return std::stoll(str);
    }
    catch(const std::invalid_argument & ){
        throw CastException("Failed to convert '" + str + "' to integer");
    }
    catch(...){
        throw;
    }
}

std::string value::real_to_string(double d) const{
    std::stringstream out;
    out << d;

    return out.str();
}

std::string value::bool_to_string(bool b) const{
    std::stringstream out;
    out << std::boolalpha;
    out << b;

    return out.str();
}

}//namespace

