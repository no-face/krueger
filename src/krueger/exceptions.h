#ifndef KRUEGER_EXCEPTIONS_H
#define KRUEGER_EXCEPTIONS_H

#include <commonex/commonex.h>

namespace krueger {

EXCEPTION_CLASS(CastException);

using commonex::IllegalStateException;

}
#endif // EXCEPTIONS_H
