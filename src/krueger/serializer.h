#ifndef KRUEGER_SERIALIZER_H
#define KRUEGER_SERIALIZER_H

#include <ostream>

#include "value.h"

namespace krueger {

class serializer{
public:
    virtual void serialize(const krueger::object & obj, std::ostream & out)=0;
    virtual void serialize(const krueger::list & lst, std::ostream &out)=0;
};

class serializer_streamer{

};

}//namespace

krueger::serializer_streamer operator<<(std::ostream & out, krueger::serializer & serializer);

template<typename T>
krueger::serializer_streamer && operator<<(krueger::serializer_streamer && out, const T & value){
    return std::move(out);
}

#endif // KRUEGER_SERIALIZER_H
