#ifndef KRUEGER_VALUE_H
#define KRUEGER_VALUE_H

#include <string>
#include <map>
#include <initializer_list>
#include <memory>

namespace krueger {

class CastException;
class object;

template <typename T>
struct is_integer_t {
    static const bool value = std::is_integral< T >::value && !std::is_same<T, bool>::value;
};

class value{
public: //types
    using real_t = double;
    using integer_t = long long;

    using opt_object = std::shared_ptr<object>;

    struct Data{
        union{
            real_t real;
            integer_t integer;
            bool boolean;
        } number;
        std::string strValue;
        opt_object object;

        Data() = default;
        Data(const Data & other);
        Data(Data && other) = default;
        Data & operator=(const Data & other);
        Data & operator=(Data && other) = default;

        void copy(const value::Data &other);
    };

    enum class Type{
        null = 0,
        boolean,
        integer,
        real,
        text,
        object
    };

public:
    friend std::ostream& operator<<(std::ostream & out, const value & v);

public:
    value() = default;
    value(std::nullptr_t nullvalue);
    value(bool b);

    value(integer_t i);
    value(real_t r);

    value(const char * cstr);
    value(std::string && str);
    value(const std::string & str);

    value(const object & obj);
    value(object && obj);

    //overloads for integers
    template<typename Integer
                ,typename  = typename std::enable_if<is_integer_t<Integer>::value>::type>
    value(Integer n)
        : value((integer_t)n)
    {}

public:
    Type type() const;
    bool is(Type t) const;
    bool empty() const;

public:
    void streamTo(std::ostream & out) const;

public: //comparison
    bool operator==(const std::string & other) const;
    bool operator==(const char * other) const;
    bool operator==(integer_t other) const;
    bool operator==(double other) const;
    bool operator==(bool other) const;

    bool operator==(const value & other) const;

    template<typename Integer,
             typename = typename std::enable_if<is_integer_t<Integer>::value>::type>
    bool operator==(Integer i){
        return type() == Type::integer && this->integer() == i;
    }

public: //casts
    operator integer_t()    const { return to_integer();}
    operator float()        const { return to_float();}
    operator double()       const { return to_double();}
    operator bool()         const { return to_bool();}
    operator std::string()  const { return to_string();}

    template<typename Integer, typename = typename std::enable_if<is_integer_t<Integer>::value>::type>
    operator Integer() const{ return to_integral<Integer>(); }

public: //conversions
    integer_t   to_integer() const;
    real_t      to_real() const;
    float       to_float() const;
    double      to_double() const;
    bool        to_bool() const;
    std::string to_string() const;

    template<typename Integer,
             typename = typename std::enable_if<is_integer_t<Integer>::value>::type>
    Integer to_integral() const{
        return (Integer)to_integer();
    }

public: //access values
    integer_t integer() const;
    real_t real() const;
    bool boolean() const;
    const std::string & str() const;

    object & obj();
    const object & obj() const;

protected:
    object & get_obj();

protected:
    bool text_to_bool(const  std::string & str) const;
    real_t text_to_real(const std::string & str) const;
    integer_t text_to_integer(const std::string & str) const;

    std::string real_to_string(double d) const;
    std::string bool_to_string(bool b) const;

protected:
    Type _type=Type::null;
    Data data;
};

class list{

};

}//namespace

#endif
