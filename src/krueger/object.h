#ifndef KRUEGER_OBJECT_H
#define KRUEGER_OBJECT_H

#include "value.h"

#include <map>

namespace krueger {

class object{
public:
    using Key = std::string;
    using ValuesMap = std::map<Key, value>;
    using Member = ValuesMap::value_type;

    using InitializerList = std::initializer_list<Member>;

    using iterator = ValuesMap::iterator;
    using const_iterator = ValuesMap::const_iterator;

public:
    object() = default;
    object(InitializerList attributes);
    object(const value & v);

    value & operator[](const Key & key);

    bool remove(const Key & key);
    void clear();

    bool contains(const Key & k) const;

    bool operator==(const object & other) const;
    bool operator!=(const object & other) const;

    bool empty() const;
    std::size_t size() const;

public:
    iterator begin();
    iterator end();
    const_iterator begin() const;
    const_iterator end() const;

protected:
    ValuesMap members;
};

}//namespace

#endif // OBJECT_H
