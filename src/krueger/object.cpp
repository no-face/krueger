#include "object.h"

#include "exceptions.h"

namespace krueger {

object::object(InitializerList attributes)
    : members(attributes)
{}

object::object(const value &v)
 : object()
{
    if(v.type() == value::Type::object){
        *this = v.obj();
    }
    else if(v.type() != value::Type::null){
        throw CastException("Failed to convert value to object");
    }
}

value & object::operator[](const std::string &str){
    return this->members[str];
}

bool object::remove(const object::Key &key){
    auto iter = this->members.find(key);
    if(iter != this->members.end()){
        this->members.erase(iter);
        return true;
    }
    return false;
}

void object::clear(){
    members.clear();
}

bool object::contains(const object::Key &k) const{
    return members.find(k) != members.end();
}

bool object::operator==(const object &other) const{
    return this->members == other.members;
}

bool object::operator!=(const object &other) const{
    return !operator ==(other);
}

bool object::empty() const{
    return members.empty();
}

std::size_t object::size() const{
    return members.size();
}

object::iterator object::begin(){
    return members.begin();
}

object::iterator object::end(){
    return members.end();
}

object::const_iterator object::begin() const{
    return members.begin();
}

object::const_iterator object::end() const{
    return members.end();
}


}//namespace
