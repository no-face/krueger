#include "helpers/TestMacros.h"

#include "krueger/krueger.h"

#include <iostream>

#define TEST_TAG "[Api]"
#define FIXTURE ApiFixture

struct Foo{
    std::string name;
    int value;
};

void parse(krueger::object & obj, const Foo & foo){
    obj["name"] = foo.name;
    obj["value"] = foo.value;
}

class FooSerializer: public krueger::serializer{
public:
    void serialize(const krueger::object & obj, std::ostream & out) override{
        this->capturedObject = obj;
    }
    void serialize(const krueger::list & lst, std::ostream &out) override{

    }
    void deserialize(std::istream & in, krueger::value & out){

    }

    krueger::object capturedObject;
};

class ApiFixture{
public:
    void test_object_serialization();
};

/* ****************************************************************************/

FIXTURE_SCENARIO("Serialization"){test_object_serialization();}

/* ****************************************************************************/

void ApiFixture::test_object_serialization(){

GIVEN("a serializer instance"){
    FooSerializer serializer;

GIVEN("a serializable entity"){
    Foo foo{"sample", 10};

WHEN("serializing it"){
    std::cout << serializer << foo;

THEN("the serializer should be called with the entity converted as an object"){
    CHECK_FALSE(serializer.capturedObject.empty());

AND_THEN("the entity fields should correspond to the converted one"){
    CHECK(serializer.capturedObject["name"] == foo.name);
    CHECK(serializer.capturedObject["value"] == foo.value);
}
}
}
}
}
}//test
