#include "helpers/TestMacros.h"

#include "krueger/values.h"
#include "krueger/exceptions.h"

using namespace krueger;

#define TEST_TAG "[value]"
#define FIXTURE ValueFixture

class ValueFixture{
public:
    void test_empty();
    void test_cast();
    void test_type();
    void test_cross_conversions();
    void test_invalid_conversions();
    void test_equals();
    void test_attribution();
};

/* ****************************************************************************/

FIXTURE_SCENARIO("test empty value"){test_empty();}
FIXTURE_SCENARIO("cast value"){test_cast();}
FIXTURE_SCENARIO("cross conversions"){test_cross_conversions();}
FIXTURE_SCENARIO("invalid conversions"){test_invalid_conversions();}
FIXTURE_SCENARIO("type should be set on constructor"){test_type();}
FIXTURE_SCENARIO("value should support '==' operator"){test_equals();}
FIXTURE_SCENARIO("value attribution"){test_attribution();}

/* ****************************************************************************/

void ValueFixture::test_empty(){
    CHECK(value().empty());
    CHECK_FALSE(value("text").empty());
    CHECK_FALSE(value(2).empty());
}

void ValueFixture::test_cast(){
    CHECK((int)         value(10u) == 10);
    CHECK((int)         value(10)       == 10);
    CHECK((long long)   value(1LL<<50)  == 1LL<<50);
    CHECK((std::string) value("hello")  == "hello");
    CHECK((bool)        value(true)     == true);
    CHECK((bool)        value(false)    == false);
    CHECK((double)      value(10.2)     == 10.2);
    CHECK((float)       value(10.2f)    == 10.2f);

    SECTION("cast to object"){
        object obj{
          {"foo", "bar"},
          {"bazz", true}
        };

        object res = value(obj);
        CHECK(res["foo"] == obj["foo"]);
        CHECK(res["bazz"] == obj["bazz"]);

        CHECK((object) value(obj) == obj);
    }
}

void ValueFixture::test_type(){
    CHECK(value().type()        == value::Type::null);
    CHECK(value(10).type()      == value::Type::integer);
    CHECK(value(1L<<10).type()  == value::Type::integer);
    CHECK(value(10.1).type()    == value::Type::real);
    CHECK(value(100.f).type()   == value::Type::real);
    CHECK(value("text").type()  == value::Type::text);
    CHECK(value(true).type()    == value::Type::boolean);
    CHECK(value(object()).type()== value::Type::object);
    CHECK(value(nullptr).type() == value::Type::null);
}

void ValueFixture::test_cross_conversions(){
    SECTION("conversions to string"){
        CHECK((std::string)value()      == "null");
        CHECK((std::string)value(1)     == "1");
        CHECK((std::string)value(-0.12) == "-0.12");
        CHECK((std::string)value(false) == "false");
    }

    SECTION("conversions to integer"){
        CHECK((int)value()        == 0);
        CHECK((int)value(123.456) == 123);
        CHECK((int)value(true)    == 1L);
        CHECK((int)value("-12")   == -12);
    }

    SECTION("conversions to double"){
        CHECK((double)value()        == 0.0);
        CHECK((double)value(-1)      == -1.0);
        CHECK((double)value(true)    == 1.0);
        CHECK((double)value("-0.01") == -0.01);
    }

    SECTION("conversions to boolean"){
        CHECK((bool)value()        == false);
        CHECK((bool)value(-1)      == true);
        CHECK((bool)value(0.0)     == false);
        CHECK((bool)value(100.0)   == true);

        bool f = (bool)value("false");
        bool t = (bool)value("true");
        CHECK_FALSE(f);
        CHECK(t);
    }
}

void ValueFixture::test_invalid_conversions(){
    CHECK_THROWS_AS((int)value("hello"), CastException);
    CHECK_THROWS_AS((double)value("hello"), CastException);
    CHECK_THROWS_AS((bool)value("hello"), CastException);
    CHECK_THROWS_AS((object)value("hello"), CastException);
}

void ValueFixture::test_equals(){
    CHECK(value(-1)      == value(-1));
    CHECK(value(1.0)     == value(1.0));
    CHECK(value(true)    == value(true));
    CHECK(value("hello") == value("hello"));
}

void ValueFixture::test_attribution(){
    value v;

    SECTION("change value"){
        v = 1;
        CHECK(v == 1);

        v = "hello";
        CHECK(v == "hello");

        v = true;
        CHECK(v == true);

        v = -123.5;
        CHECK(v == -123.5);

        v = object{{"hello", "world"}};
        CHECK(v.obj().contains("hello"));
    }

    SECTION("value should keep only one value type"){
        v = 10;
        v = true;
        v = "abc";
        v = 12.0f;
        v = object();

        CHECK(v.integer() == 0);
        CHECK(v.boolean() == false);
        CHECK(v.str().empty());
        CHECK(v.real() == 0.0);
    }

    SECTION("object should be copied not referenced"){
        value v2 = object{{"hello","world"}};
        v = v2;

        CHECK(v == v2);

        v.obj()["hello"] = 12;

        CHECK(v.obj()["hello"] == 12);
        CHECK(v2.obj()["hello"] == "world");
    }
}

