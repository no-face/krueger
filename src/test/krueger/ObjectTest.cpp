#include "helpers/TestMacros.h"

#include "krueger/object.h"

using namespace krueger;

#define TEST_TAG "[object]"
#define FIXTURE ObjectFixture

class ObjectFixture{
public:
    void test_object_populate();
    void test_object_extract();
    void test_object_equals();
    void test_object_contains();
    void test_remove_member();
    void test_empty();
    void test_size();
    void test_iterate_members();
};

/* ****************************************************************************/

FIXTURE_SCENARIO("object population")           {test_object_populate();}
FIXTURE_SCENARIO("extract from object")         {test_object_extract();}
FIXTURE_SCENARIO("object equals")               {test_object_equals();}
FIXTURE_SCENARIO("object contains")             {test_object_contains();}
FIXTURE_SCENARIO("remove member from object")   {test_remove_member();}
FIXTURE_SCENARIO("empty object")                {test_empty();}
FIXTURE_SCENARIO("iterate over members")        {test_iterate_members();}

/* ****************************************************************************/

void ObjectFixture::test_object_populate(){
    object obj;
    obj["inteiro"] = 10;
    obj["texto"] = "hello";
    obj["bool"] = true;
    obj["real"] = -1.0;

    CHECK(obj["inteiro"] == 10);
    CHECK(obj["texto"] == "hello");
    CHECK(obj["bool"] == true);
    CHECK(obj["real"] == -1.0);
}

void ObjectFixture::test_object_extract(){
    object obj{
        {"i", 200},
        {"t", "world"},
        {"b", false},
        {"r", 0.01}
    };

    int i = obj["i"];
    std::string t = obj["t"];
    bool b = obj["b"];
    double r = obj["r"];

    CHECK(i == 200);
    CHECK(t == "world");
    CHECK(b == false);
    CHECK(r == 0.01);
}

void ObjectFixture::test_object_equals(){
    object o1{{"a", 1}, {"b", true}, {"c", "lorem ipsum"}, {"d", -0.02}};
    object o2{{"d", -0.02}, {"c", "lorem ipsum"}, {"b", true}, {"a", 1} };

    SECTION("objects are equals"){
        CHECK(o1["a"] == o2["a"]);
        CHECK(o1["b"] == o2["b"]);
        CHECK(o1["c"] == o2["c"]);
        CHECK(o1["d"] == o2["d"]);

        CHECK(o1 == o2);
    }

    SECTION("objects are different"){
        object o3{{"c", "lorem ipsum"}, {"b", true}, {"a", 1} };
        object o4;

        CHECK_FALSE(o1 == o3);
        CHECK_FALSE(o1 == o4);
        CHECK_FALSE(o3 == o4);

        CHECK(o1 != o3);
        CHECK(o1 != o4);
        CHECK(o3 != o4);
    }
}

void ObjectFixture::test_object_contains(){
    object o1{{"a", 1}};

    CHECK(o1.contains("a"));
    CHECK_FALSE(o1.contains("A"));
    CHECK_FALSE(o1.contains(""));
}

void ObjectFixture::test_remove_member(){
    object obj{{"a", 1}, {"b", 2}};

    CHECK(obj.remove("a"));
    CHECK_FALSE(obj.contains("a"));
    CHECK_FALSE(obj.remove("a"));

    CHECK(obj.remove("b"));
    CHECK_FALSE(obj.contains("b"));
    CHECK_FALSE(obj.remove("b"));

    CHECK_FALSE(obj.remove("sample"));
}

void ObjectFixture::test_empty(){
    GIVEN("an default initialized object"){
        object obj;

        THEN("it should be empty"){
            CHECK(obj.empty());
        }

    WHEN("adding a member to it"){
        obj["a"] = true;

        THEN("it should not be empty anymore"){
            CHECK_FALSE(obj.empty());
        }

    WHEN("the member is removed"){
        obj.remove("a");

    THEN("the object should be empty"){
        CHECK(obj.empty());
    }
    }
    }
    }
}//test

void ObjectFixture::test_size(){
    object obj{{"a", ""}, {"b", 0}};

    CHECK(obj.size() == 2);

    obj.clear();

    CHECK(obj.size() == 0);
}

void ObjectFixture::test_iterate_members(){
    object o1{{"a", 1}, {"b", true}, {"c", "lorem ipsum"}, {"d", -0.02}};
    object copy;

    int count = 0;
    for(const auto m : o1){
        copy[m.first] = m.second;
        ++count;
    }

    CHECK(count == o1.size());
    CHECK(o1 == copy);
}

